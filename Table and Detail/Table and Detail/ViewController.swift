//
//  ViewController.swift
//  Table and Detail
//
//  Created by Wylie, Louise on 09/01/2021.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var currentCell = -1
    
    var staff = [("Phil", "A1.20", "phil@liv.ac.uk"),("Terry","A2.18","trp@liv.ac.uk"),("Valli","A2.12","V.Tamma@liv.ac.uk"),("Boris","A1.15","Konev@liv.ac.uk")]
    
    var selectedPerson = ("","","")
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return staff.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
        cell.textLabel!.text = staff[indexPath.row].0
        cell.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        currentCell = indexPath.row
        selectedPerson = staff[currentCell]
        performSegue(withIdentifier: "toDetailView", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            staff.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDetailView" {
            let detailViewController = segue.destination as! DetailViewController
            detailViewController.selectedPerson = staff[currentCell]
        }
    }
}

