//
//  DetailViewController.swift
//  Table and Detail
//
//  Created by Wylie, Louise on 09/01/2021.
//

import UIKit

class DetailViewController: UIViewController {

    var selectedPerson = ("","","")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        name.text = selectedPerson.0
        room.text = selectedPerson.1
        email.text = selectedPerson.2
        // Do any additional setup after loading the view.
    }
    

    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var room: UILabel!
        
    @IBOutlet weak var email: UILabel!
    
     /*
     // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
